package com.riftar.githubapp.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.riftar.githubapp.data.UserRepository
import com.riftar.githubapp.model.User
import kotlinx.coroutines.flow.Flow

class MainViewModel @ViewModelInject constructor(private val repository: UserRepository): ViewModel(){
    private var currentQueryValue: String? = null
    private var currentSearchResult: Flow<PagingData<User>>? = null
    fun searchRepo(queryString: String): Flow<PagingData<User>> {
        val lastResult = currentSearchResult
        if (queryString == currentQueryValue && lastResult != null) {
            return lastResult
        }
        currentQueryValue = queryString
        val newResult: Flow<PagingData<User>> = repository.getUserSearchResult(queryString)
            .cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }
}