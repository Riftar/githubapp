package com.riftar.githubapp.ui.main

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.riftar.githubapp.R
import com.riftar.githubapp.model.User

class UserViewHolder(view: View): RecyclerView.ViewHolder(view){
    private val name: TextView = view.findViewById(R.id.tv_user_name)
    private val photo: ImageView = view.findViewById(R.id.iv_user_image)
    private val progress: ProgressBar = view.findViewById(R.id.progress_user)
    private val viewCtx = view.context

    private var user: User? = null
    init {
        view.setOnClickListener {
            Toast.makeText(view.context, user?.login.toString(), Toast.LENGTH_LONG).show()
        }
    }

    fun bind(user: User) {
        showRepoData(user)
    }

    private fun showRepoData(user: User) {
        this.user = user
        name.text = user.login
        Glide.with(viewCtx)
            .load(user.avatarUrl)
            .transform(CenterCrop())
            .addListener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress.visibility = View.GONE
                    return false
                }
            })
            .into(photo)
    }

    companion object {
        fun create(parent: ViewGroup): UserViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user, parent, false)
            return UserViewHolder(view)
        }
    }
}