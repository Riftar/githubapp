package com.riftar.githubapp.model

import com.google.gson.annotations.SerializedName

data class GetSearchUserResponse (
    @SerializedName("total_count")
    val totalCount: Int = 0,

    @SerializedName("incomplete_results")
    val incompleteResults: Boolean,

    @SerializedName("items")
    val items: List<User> = emptyList()
)