package com.riftar.githubapp.model

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("login")
    val login: String? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("avatar_url")
    val avatarUrl: String? = null
)