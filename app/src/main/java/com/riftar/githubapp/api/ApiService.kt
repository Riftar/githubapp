package com.riftar.githubapp.api

import com.riftar.githubapp.model.GetSearchUserResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {

    @GET("search/users")
    suspend fun getUserByQuery(
        @Query("q") query: String,
        @Query("per_page") perPage: Int,
        @Query("page") page: Int
    ): Response<GetSearchUserResponse>

}