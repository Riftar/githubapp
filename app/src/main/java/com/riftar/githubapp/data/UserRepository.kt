package com.riftar.githubapp.data

import android.util.Log
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.riftar.githubapp.api.ApiService
import com.riftar.githubapp.model.User
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UserRepository  @Inject constructor(private val apiService: ApiService){
    companion object {
        const val NETWORK_PAGE_SIZE = 25
    }
    fun getUserSearchResult(query: String): Flow<PagingData<User>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false),
            pagingSourceFactory = { UserPagingSource(apiService, query) }
        ).flow
    }
}