package com.riftar.githubapp.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.riftar.githubapp.api.ApiService
import com.riftar.githubapp.data.UserRepository.Companion.NETWORK_PAGE_SIZE
import com.riftar.githubapp.model.User
import retrofit2.HttpException
import java.io.IOException

private const val GITHUB_STARTING_PAGE_INDEX = 1

class UserPagingSource(
    private val service: ApiService,
    private val query: String
) : PagingSource<Int, User>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, User> {
        val position = params.key ?: GITHUB_STARTING_PAGE_INDEX
        val apiQuery = query
        return try {
            val response = service.getUserByQuery(apiQuery, params.loadSize, position)
            if (response.isSuccessful){
                val users = response.body()?.items ?: emptyList()
                val nextKey = if (users.isEmpty()) {
                    null
                } else {
                    // initial load size = 3 * NETWORK_PAGE_SIZE
                    position + (params.loadSize / NETWORK_PAGE_SIZE)
                }
                LoadResult.Page(
                    data = users,
                    prevKey = if (position == GITHUB_STARTING_PAGE_INDEX) null else position - 1,
                    nextKey = nextKey
                )
            } else{
                val msg = when(response.code()){
                    403 -> "Slow down bro.. try again later"
                    else -> response.message()
                }
                val exception = Exception(msg)
                LoadResult.Error(exception)
            }
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        } catch (exception: Exception){
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, User>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}